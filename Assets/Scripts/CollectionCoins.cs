using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CollectionCoins : MonoBehaviour
{
    public int coins;
    private int totalCoins = 12;
    public Text text;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag =="Coin")
        {
            Debug.Log("Coin collected!");

            totalCoins = totalCoins - 1;
            text.text = totalCoins.ToString() + "/12 Coins remaining";

            coins = coins + 1;

            Destroy(other.gameObject);
        }
        if (coins == 12)
        {
            SceneManager.LoadScene(0);
        }
    }
}
